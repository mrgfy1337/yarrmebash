#!/usr/bin/env bash
# -*- coding: utf-8 -*-bash

echo "Do NOT run this script as ROOT."
sleep 1
echo "The script will prompt the user for elevated privilages when nessassery."
sleep 2
echo "Ctrl+c to exit and start over, or wait 5 second to continue installation."
sleep 5
echo "Installing Nerd fonts complete, you will need to set your terminal font via your terminals settings. I preffer 'GoMono Nerd Font' (fonts that end with mono have smaller icons)"
sleep .5
git clone https://github.com/ryanoasis/nerd-fonts.git && cd nerd-fonts && sudo -S bash install.sh
cd - || exit 1
echo "Updating font cache"
sleep .5
sudo -S fc-cache -fv
echo "Installing starship shell prompt"
sleep .5
bash -c "$(curl -fsSL https://starship.rs/install.sh)"
rm -f ~/.config/starship.toml && mv -f starship.toml ~/.config/starship.toml
echo "Installing an ls alternative, lsd and bat, an alternative to cat"
sleep .5
sudo -S pacman -S lsd bat
echo "Backing up original .bashrc to ~/.bashrc.bak and installing the new .bashrc"
cp ~/.bashrc ~/.bashrc.bak && mv bashrc ~/.bashrc
#shellcheck disable=SC1090
source ~/.bashrc
echo "Installing feh and setting wallpaper(s)"
sleep .5
sudo -S pacman -S feh
feh --bg-center ./Wallpapers/jolly-roger-wallpaper.png ./Wallpapers/yoho.piratebay.wallpaper.jpg
echo "Now set the darker wallpaper as your terminals backgroung using your terminals settings in its menu bar"
echo "All finished, I hope you enjoy the new Pirate theme. Yaaaarrr!"
