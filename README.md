# Yar Me Bash is a complete Linux bash shell prompt configuration and theme for Linux
(I made this to use the ArchLinux package manager, but feel free to edit this to work for you.)

* The files included are the .bashrc, starship.toml + a few wallpapers.
* Just run the install.sh and let the script do the rest.

## Installation

* Run without sudo or root user

```bash
./install.sh
```
   or try
```
bash install.sh
```
* Please note: the bashrc has lsd aliased to ls and bat aliased to cat. So please go over the bashrc file and comment in/out what works for you before installing.
* Once finished. If any issues are present, just comment stuff out in the new .bashrc, feh might not set wallpapers correctly, do this manually.
* And don't forget to source your new .bashrc.
```bash
. .bashrc
```

![](Screenshots/yarmearch.png)
![](Screenshots/yarmearch-2.png)
![](Screenshots/Screenshot_1.png)

## Enjoy
